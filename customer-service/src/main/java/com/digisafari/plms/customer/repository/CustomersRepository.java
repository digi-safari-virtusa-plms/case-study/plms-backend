package com.digisafari.plms.customer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.digisafari.plms.customer.domain.Customer;

@Repository
public interface CustomersRepository extends MongoRepository<Customer, String> {

	Customer findByEmail(String email);
	
}
