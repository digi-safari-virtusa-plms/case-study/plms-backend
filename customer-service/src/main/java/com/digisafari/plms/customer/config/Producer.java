package com.digisafari.plms.customer.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digisafari.plms.customer.domain.Loan;
import com.digisafari.plms.customer.rabbitmq.UserSignupDto;

@Component
public class Producer {

	@Autowired
	private RabbitTemplate rabbitMQTemplate;
	
	@Autowired
	private DirectExchange directExchange;
	
	public void sendUserSignUpDetailsToRabbitMQ(UserSignupDto userSignupDto) {
		rabbitMQTemplate.convertAndSend(directExchange.getName(),"user_register_key",userSignupDto);
	}
	public void sendUserUpdatedDetailsToRabbitMQ(UserSignupDto userSignupDto) {
		rabbitMQTemplate.convertAndSend(directExchange.getName(),"user_update_key",userSignupDto);
	}
	
	public void sendLoanDetailsToRabbitMQ(Loan loan) {
		rabbitMQTemplate.convertAndSend(directExchange.getName(),"loans_key",loan);
	}
	
	
}
