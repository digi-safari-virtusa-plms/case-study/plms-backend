package com.digisafari.plms.customer.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQMessasingConfiguration {

	private String userExchange = "user_exchange";
	private String userRegisterQueue = "user_register_queue";
	private String userUpdateQueue = "user_update_queue";
	private String loansQueue = "loans_queue";

	
	@Bean
	  DirectExchange exchange(){
	    return new DirectExchange(userExchange);

	  }

	  @Bean
	  Queue userRegisterQueue(){
	    return new Queue(userRegisterQueue , true,false,false,null);
	  }
	  
	  @Bean
	  Queue userUpdateQueue(){
	    return new Queue(userUpdateQueue , true,false,false,null);
	  }
	  
	  @Bean
	  Queue loanQueue(){
	    return new Queue(loansQueue ,  true,false,false,null);
	  }
	  
	  @Bean
	  Binding bindingRegisterQueue(Queue userRegisterQueue , DirectExchange exchange){

	    return BindingBuilder.bind(userRegisterQueue()).to(exchange).with("user_register_key");
	  }

	  @Bean
	  Binding bindingUserUpdateQueue(Queue userUpdateQueue , DirectExchange exchange){

	    return BindingBuilder.bind(userUpdateQueue()).to(exchange).with("user_update_key");
	  }

	  
	  @Bean
	  Binding bindingLoanQueue(Queue loanQueue , DirectExchange exchange){

	    return BindingBuilder.bind(loanQueue()).to(exchange).with("loans_key");
	  }
	  
	  @Bean
	  public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
	    return new Jackson2JsonMessageConverter();
	  }

	  @Bean
	  public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
	    RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
	    rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
	    return rabbitTemplate;
	  }


}
