package com.digisafari.plms.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.CONFLICT, reason = "Loan already favourited")
public class LoanAlreadyFavouritedException extends Exception {

}
