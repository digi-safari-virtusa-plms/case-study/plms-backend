package com.digisafari.plms.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.CONFLICT, reason = "Customer already exists with this email")
public class CustomerAlreadyExistsException extends Exception {

}
