package com.digisafari.plms.customer.domain;


import org.springframework.data.annotation.Id;

public class Loan {
	
	@Id
	private String id;
	private String bankName;
	private String bankImageUrl;
	private int minLoanAmount;
	private int maxLoanAmount;
	private float minInterestRate;
	private float maxInterestRate;
	private short minCreditScore;
	private byte termLength;
	private short processingFee;
	private float rating;
	private int favouriteCount;
	
	public Loan() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}



	public String getBankImageUrl() {
		return bankImageUrl;
	}

	public void setBankImageUrl(String bankImageUrl) {
		this.bankImageUrl = bankImageUrl;
	}

	public int getMinLoanAmount() {
		return minLoanAmount;
	}

	public void setMinLoanAmount(int minLoanAmount) {
		this.minLoanAmount = minLoanAmount;
	}

	public int getMaxLoanAmount() {
		return maxLoanAmount;
	}

	public void setMaxLoanAmount(int maxLoanAmount) {
		this.maxLoanAmount = maxLoanAmount;
	}

	public float getMinInterestRate() {
		return minInterestRate;
	}

	public void setMinInterestRate(float minInterestRate) {
		this.minInterestRate = minInterestRate;
	}

	public float getMaxInterestRate() {
		return maxInterestRate;
	}

	public void setMaxInterestRate(float maxInterestRate) {
		this.maxInterestRate = maxInterestRate;
	}

	public short getMinCreditScore() {
		return minCreditScore;
	}

	public void setMinCreditScore(short minCreditScore) {
		this.minCreditScore = minCreditScore;
	}

	public byte getTermLength() {
		return termLength;
	}

	public void setTermLength(byte termLength) {
		this.termLength = termLength;
	}

	public short getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(short processingFee) {
		this.processingFee = processingFee;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getFavouriteCount() {
		return favouriteCount;
	}

	public void setFavouriteCount(int favouriteCount) {
		this.favouriteCount = favouriteCount;
	}

	@Override
	public String toString() {
		return "Loan [id=" + id + ", bankName=" + bankName + ", bankImageUrl=" + bankImageUrl + ", minLoanAmount="
				+ minLoanAmount + ", maxLoanAmount=" + maxLoanAmount + ", minInterestRate=" + minInterestRate
				+ ", maxInterestRate=" + maxInterestRate + ", minCreditScore=" + minCreditScore + ", termLength="
				+ termLength + ", processingFee=" + processingFee + ", rating=" + rating + ", favouriteCount="
				+ favouriteCount + "]";
	}

	
	

}
