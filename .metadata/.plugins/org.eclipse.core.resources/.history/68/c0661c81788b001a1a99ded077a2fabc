package com.digisafari.plms.loan.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digisafari.plms.loan.domain.Loan;
import com.digisafari.plms.loan.exception.LoanNotFoundException;
import com.digisafari.plms.loan.repository.LoanRepository;

@Service
public class LoanService implements ILoanService {

	@Autowired
	private LoanRepository loanRepository;
	
	@Override
	public Loan getLoanById(String loanId) throws LoanNotFoundException {
		Loan loan = null;
		try {
			Optional<Loan> loanByIdOptional = this.loanRepository.findById(loanId);
			if(loanByIdOptional.isPresent()) {
				loan = loanByIdOptional.get();
			} else {
				throw new LoanNotFoundException();
			}
			
		} catch (LoanNotFoundException e) {
			throw e;
		} 
		return loan;
	}

	@Override
	public List<Loan> getLoansList() {
		return this.loanRepository.findAll();
	}

	@Override
	public Loan updateLoan(Loan loan) throws LoanNotFoundException {
		Loan updatedLoan = null;
		try {
			Optional<Loan> loanByIdOptional = this.loanRepository.findById(loan.getId());
			if(!loanByIdOptional.isPresent()) {
				throw new LoanNotFoundException();
			} else {
				updatedLoan = this.loanRepository.save(loan);
			}
			
		} catch (LoanNotFoundException e) {
			throw e;
		} 
		return updatedLoan;
	}

}
