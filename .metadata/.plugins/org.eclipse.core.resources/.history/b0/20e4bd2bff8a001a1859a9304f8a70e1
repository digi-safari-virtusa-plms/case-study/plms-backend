package com.digisafari.plms.customer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digisafari.plms.customer.domain.Customer;
import com.digisafari.plms.customer.domain.CustomerLoan;
import com.digisafari.plms.customer.domain.Loan;
import com.digisafari.plms.customer.exception.CustomerAlreadyExistsException;
import com.digisafari.plms.customer.exception.CustomerLoanNotFoundException;
import com.digisafari.plms.customer.exception.CustomerNotFoundException;
import com.digisafari.plms.customer.exception.LoanAlreadyAppliedException;
import com.digisafari.plms.customer.exception.LoanAlreadyFavouritedException;
import com.digisafari.plms.customer.exception.LoanNotFoundException;
import com.digisafari.plms.customer.repository.CustomersRepository;

@Service
public class CustomerService implements ICustomerService {

	@Autowired
	private CustomersRepository customersRepository;
	
	@Override
	public List<Customer> getCustomers() {		
		return this.customersRepository.findAll();
	}

	@Override
	public Customer getCustomerById(String id) throws CustomerNotFoundException {
		Customer customer = null;
		try {
			Optional<Customer> customerByIdOptional = this.customersRepository.findById(id);
			if(customerByIdOptional.isPresent()) {
				customer = customerByIdOptional.get();
			} else {
				throw new CustomerNotFoundException();
			}
			
		} catch (CustomerNotFoundException e) {
			throw e;
		} 
		return customer;
	}

	@Override
	public Customer updateCustomer(Customer customer) throws CustomerNotFoundException {		
		Customer updatedCustomer = null;
		try {
			Optional<Customer> customerByIdOptional = this.customersRepository.findById(customer.getId());
			if(!customerByIdOptional.isPresent()) {
				throw new CustomerNotFoundException();
			} else {
				updatedCustomer = this.customersRepository.save(customer);
			}
			
		} catch (CustomerNotFoundException e) {
			throw e;
		} 
		return updatedCustomer;
	}

	@Override
	public List<CustomerLoan> getCustomerLoans(String customerId) throws CustomerNotFoundException {		
		List<CustomerLoan> customerAppliedLoans = null;
		Customer customer = null;
		try {
			 customer = getCustomerById(customerId);
			 customerAppliedLoans = customer.getAppliedLoans();
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerAppliedLoans;
	}

	@Override
	public Customer signupCustomer(Customer customer) throws CustomerAlreadyExistsException {
		Customer createdCustomer = null;
		try {
			//customer.setProfilePic(new Binary(BsonBinarySubType.BINARY,profilePic.getBytes()));
		    boolean isCustomerAlreadyExists = checkCustomerAlreadyExistsWithEmail(customer.getEmail());
			if(isCustomerAlreadyExists == true) {
				throw new CustomerAlreadyExistsException();
			}
		    createdCustomer = this.customersRepository.save(customer);
		
		} catch(CustomerAlreadyExistsException e) {
			throw e;
		}
		
		return createdCustomer;
	}

	@Override
	public boolean checkCustomerAlreadyExistsWithEmail(String email) throws CustomerAlreadyExistsException {		
		boolean isExists = false;
		try {
			Customer customer = this.customersRepository.findByEmail(email);
			if(customer != null) {
				isExists = true;
				throw new CustomerAlreadyExistsException();
			}
		}  catch(CustomerAlreadyExistsException e) {
			throw e;
		}
		return isExists;
		
	}

	@Override
	public List<Loan> getCustomerFavLoans(String customerId) throws CustomerNotFoundException {
		List<Loan> customerFavLoansList = null;
		try {
			Customer customer = getCustomerById(customerId);
			customerFavLoansList = customer.getFavLoans();
		} catch (CustomerNotFoundException e) {
			throw e;
		}
		return customerFavLoansList;
	}

	@Override
	public Loan addLoanToCustomerFavList(String customerId, Loan loanToBeFavourited) throws CustomerNotFoundException, LoanAlreadyFavouritedException {
		Customer customer = null;
		try {
			 customer = getCustomerById(customerId);
			 System.out.println("customer" + customer );
			 System.out.println("Loan to Be Favourited" + loanToBeFavourited);
			 
			 List<Loan> customerFavLoansList = customer.getFavLoans();
			 System.out.println("Favourite Loan List" + customerFavLoansList);
			/*
			 * Loan favouritedLoan = customerFavLoansList.stream(). filter(loan ->
			 * loan.getId() == loanToBeFavourited.getId()). findFirst(). orElse(null);;
			 */
			 boolean alreadyFavourited = customerFavLoansList.stream().anyMatch(loan ->
			             loan.getId().equalsIgnoreCase(loanToBeFavourited.getId()));
					 System.out.println("Favourited Loan"+alreadyFavourited);
			 if(alreadyFavourited == false) {
			 customerFavLoansList.add(loanToBeFavourited);
			 customer.setFavLoans(customerFavLoansList);
			 customer = this.customersRepository.save(customer);
			 }
			 else {
				 throw new LoanAlreadyFavouritedException();
			 }
		} catch (CustomerNotFoundException e) {
			throw e;
		}
		catch (LoanAlreadyFavouritedException e) {
			throw e;
		}
		return loanToBeFavourited;
	}	

	

	@Override
	public boolean removeLoanFromCustomerFavList(String customerId, Loan loanToBeRemoved) throws CustomerNotFoundException,LoanNotFoundException {
		
		boolean status = false;
		try {
			 Customer customer = getCustomerById(customerId);
			 List<Loan> customerFavLoansList = customer.getFavLoans();
			 boolean removed = customerFavLoansList.removeIf(loan -> loan.getId().equalsIgnoreCase(loanToBeRemoved.getId()));
			 if(removed) {
				 customer.setFavLoans(customerFavLoansList);
				 customer = this.customersRepository.save(customer);
				 status = true;
			 } else {
				 throw new LoanNotFoundException();
			 }
		} catch (CustomerNotFoundException e) {
			throw e;
		} catch( LoanNotFoundException e) {
			throw e;
		}
		return status;
	}

	/*
	 * @Override public CustomerLoan checkCustomerAlreadyAppliedForLoan(String
	 * loanId, String customerId) throws LoanAlreadyAppliedException,
	 * CustomerNotFoundException { CustomerLoan appliedLoan = null; Customer
	 * customer = null; try { customer = getCustomerById(customerId); appliedLoan =
	 * customer.getAppliedLoans().stream(). filter(loan -> loan.getId() == loanId).
	 * findAny(). orElse(null); if(appliedLoan != null) { throw new
	 * LoanAlreadyAppliedException(); } } catch (CustomerNotFoundException e) {
	 * e.printStackTrace(); throw e; } catch(LoanAlreadyAppliedException e) { throw
	 * e; }
	 * 
	 * return appliedLoan; }
	 */
	
	@Override
	public CustomerLoan applyNewLoan(String customerId, CustomerLoan customerLoan) throws CustomerNotFoundException,LoanAlreadyAppliedException {
			
		try {
			 Customer customer = getCustomerById(customerId);
			 List<CustomerLoan> appliedLoans = customer.getAppliedLoans();
			 System.out.println("Applied LOans" + appliedLoans);
			 CustomerLoan appliedLoan = appliedLoans.stream().
			 filter(loan -> loan.getId().equalsIgnoreCase(customerLoan.getId())).
			 findAny().
			 orElse(null);
			 System.out.println("Applied LOan" + appliedLoan);
			 if(appliedLoan != null) {
				 throw new LoanAlreadyAppliedException();
			 }
			 else {
				 appliedLoans.add(customerLoan);
				 customer.setAppliedLoans(appliedLoans);
				 customer = this.customersRepository.save(customer);
			 }
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch(LoanAlreadyAppliedException e) {
			throw e;
		}
		
		return customerLoan;
	}

	@Override
	public CustomerLoan approveCustomerLoan(String customerId, String loanId)
			throws CustomerNotFoundException, CustomerLoanNotFoundException {		
		try {
			 Customer customer = getCustomerById(customerId);
			 List<CustomerLoan> appliedLoans = customer.getAppliedLoans();
			 System.out.println("Applied Loans" + appliedLoans);
			 CustomerLoan appliedLoan = appliedLoans.stream().
			 filter(loan -> loan.getId().equalsIgnoreCase(loanId)).
			 findAny().			 
			 orElse(null);
			 System.out.println("Applied Loan" + appliedLoan);
			 if(appliedLoan != null) {				
				appliedLoan.setVerificationStatus("Verification Successful");
				appliedLoan.setStatus("Approved");	
				this.customersRepository.save(customer);
			 }
			 else {
				throw new CustomerLoanNotFoundException();
			 }
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch(CustomerLoanNotFoundException e) {
			throw e;
		}
		
		return customerLoan;
	}

	@Override
	public CustomerLoan rejectCustomerLoan(String customerId, String loanId)
			throws CustomerNotFoundException ,CustomerLoanNotFoundException{
		return null;
	}

	
	

	
}
