package com.digisafari.plms.customer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digisafari.plms.customer.domain.Customer;
import com.digisafari.plms.customer.domain.CustomerLoan;
import com.digisafari.plms.customer.domain.Loan;
import com.digisafari.plms.customer.exception.CustomerAlreadyExistsException;
import com.digisafari.plms.customer.exception.CustomerLoanNotFoundException;
import com.digisafari.plms.customer.exception.CustomerNotFoundException;
import com.digisafari.plms.customer.exception.LoanAlreadyAppliedException;
import com.digisafari.plms.customer.exception.LoanAlreadyFavouritedException;
import com.digisafari.plms.customer.exception.LoanNotFoundException;
import com.digisafari.plms.customer.service.CustomerService;

@RestController
@CrossOrigin("*")
@RequestMapping("api/v1/customers")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	ResponseEntity<?> responseEntity;
	
	@PostMapping("/signup")
	public ResponseEntity<?> signUpCustomer(@RequestBody Customer customer) throws CustomerAlreadyExistsException{
	
		try {
			Customer createdCustomer = this.customerService.signupCustomer(customer);
			responseEntity = new ResponseEntity<>(createdCustomer, HttpStatus.CREATED);
		}
		
		catch (CustomerAlreadyExistsException e) {
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
		}		
		
		return responseEntity;
	}
	
	@GetMapping("/customers")
	public ResponseEntity<?> getCustomers() {
		List<Customer> customersList = null;
		try {
			customersList = this.customerService.getCustomers();
			responseEntity = new ResponseEntity<>(customersList, HttpStatus.OK);
		}
		
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return responseEntity;
	}
	
	@GetMapping("/customers/{id}")
	public ResponseEntity<?> getCustomerById(@PathVariable("id") String id) throws CustomerNotFoundException {
		Customer customer = null;
		try {
			customer = this.customerService.getCustomerById(id);
			responseEntity = new ResponseEntity<>(customer, HttpStatus.OK);
		}catch (CustomerNotFoundException e) {
			throw e;
		}
		
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return responseEntity;
	}
	
	
	@PutMapping("/customers")
	public ResponseEntity<?> updateCustomer(@RequestBody Customer customer) throws CustomerNotFoundException{
		
		try {
			Customer updatedCustomer = this.customerService.updateCustomer(customer);
			responseEntity = new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
		}		
		catch (CustomerNotFoundException e) {
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
		}		
		
		return responseEntity;
	}
	
	@GetMapping("/customers/{customerId}/appliedloans")
	public ResponseEntity<?> getCustomerAppliedLoans(@PathVariable("customerId") String customerId) throws CustomerNotFoundException {
		List<CustomerLoan> customerAppliedLoans = null;
		try {
			customerAppliedLoans = this.customerService.getCustomerLoans(customerId);
			responseEntity = new ResponseEntity<>(customerAppliedLoans, HttpStatus.OK);
		}catch (CustomerNotFoundException e) {
			throw e;
		}
		
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return responseEntity;
	}
	
	@GetMapping("/isexists")
	public ResponseEntity<?> checkCustomerAlreadyExistsWithEmail(@RequestParam("email") String email)  {
		boolean isExists = false;
		try {
			System.out.println("email:::" + email);
			isExists = this.customerService.checkCustomerAlreadyExistsWithEmail(email);
			responseEntity = new ResponseEntity<>(isExists, HttpStatus.OK);
		}		
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return responseEntity;
	}
	
	@GetMapping("/customers/{customerId}/favloans")
	public ResponseEntity<?> getCustomerFavLoans(@PathVariable("customerId") String customerId) throws CustomerNotFoundException {
		List<Loan> customerFavLoans = null;
		try {
			customerFavLoans = this.customerService.getCustomerFavLoans(customerId);
			responseEntity = new ResponseEntity<>(customerFavLoans , HttpStatus.OK);
		}catch (CustomerNotFoundException e) {
			throw e;
		}
		
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return responseEntity;
	}
	
	@PostMapping("/customers/{customerId}/favourites")
	public ResponseEntity<?> addLoanToCustomerFavList(@PathVariable("customerId") String customerId, @RequestBody Loan loan) throws CustomerNotFoundException,LoanAlreadyFavouritedException{
		Loan loanFavourited = null;
		try {
			loanFavourited = this.customerService.addLoanToCustomerFavList(customerId, loan);
			responseEntity = new ResponseEntity<>(loanFavourited,HttpStatus.OK);
		}
			catch (CustomerNotFoundException e) {
				throw e;
			}
			catch (LoanAlreadyFavouritedException e) {
				throw e;
			} 
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@DeleteMapping("/customers/{customerId}/favourites")
	public ResponseEntity<?> removeLoanFromCustomerFavList(@PathVariable("customerId") String customerId, @RequestBody Loan loanToBeUnFavourited) throws CustomerNotFoundException, LoanNotFoundException{
		boolean status = false;
		try {
			status = this.customerService.removeLoanFromCustomerFavList(customerId, loanToBeUnFavourited);
			responseEntity = new ResponseEntity<>(status , HttpStatus.OK);
		}
			catch (CustomerNotFoundException e) {
				throw e;
			}
			catch (LoanNotFoundException e) {
				throw e;
			} 
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/customers/{customerId}/appliedloan/{loanId}")
	public ResponseEntity<?> checkCustomerAlreadyAppliedForLoan(@PathVariable("customerId") String customerId,@PathVariable("loanId") String loanId) throws CustomerNotFoundException{
			boolean appliedStatus = false;
			CustomerLoan appliedLoan = null;
			try {
				appliedLoan = this.customerService.checkCustomerAlreadyAppliedForLoan(loanId, customerId);	
				System.out.println("Applied Loan" + appliedLoan);
				responseEntity = new ResponseEntity<>(appliedStatus,HttpStatus.OK);
				
			}
				catch (CustomerNotFoundException e) {
					throw e;
				}
				catch (LoanAlreadyAppliedException e) {
					appliedStatus = true;
					System.out.println("Applied Status" + appliedStatus);
					responseEntity = new ResponseEntity<>(appliedStatus,HttpStatus.OK);
				} 
			catch (Exception e) {
				e.printStackTrace();
				responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			return responseEntity;
	}
	
	
	@PostMapping("/customers/{customerId}/applyloan")
	public ResponseEntity<?> applyNewLoan(@PathVariable("customerId") String customerId, @RequestBody CustomerLoan customerLoan) throws CustomerNotFoundException, LoanAlreadyAppliedException{
	
		CustomerLoan appliedLoan = null;
		try {
			appliedLoan = this.customerService.applyNewLoan(customerId, customerLoan);
			responseEntity = new ResponseEntity<>(appliedLoan,HttpStatus.OK);
		}
			catch (CustomerNotFoundException e) {
				throw e;
			}
			catch (LoanAlreadyAppliedException e) {
				throw e;
			} 
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/customers/{customerId}/approveloan/{loanId}")
	public ResponseEntity<?> approveLoan(@PathVariable("customerId") String customerId, @PathVariable("loanId") String loanId) throws CustomerNotFoundException, CustomerLoanNotFoundException{
		
		CustomerLoan appliedLoan = null;
		try {
			appliedLoan = this.customerService.approveCustomerLoan(customerId, loanId);
			responseEntity = new ResponseEntity<>(appliedLoan,HttpStatus.OK);
		}
			catch (CustomerNotFoundException e) {
				throw e;
			}
			catch (CustomerLoanNotFoundException e) {
				throw e;
			} 
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PutMapping("/customers/{customerId}/rejectloan/{loanId}")
	public ResponseEntity<?> rejectLoan(@PathVariable("customerId") String customerId, @PathVariable("loanId") String loanId) throws CustomerNotFoundException, CustomerLoanNotFoundException{
		
		CustomerLoan appliedLoan = null;
		try {
			appliedLoan = this.customerService.rejectCustomerLoan(customerId, loanId);
			responseEntity = new ResponseEntity<>(appliedLoan,HttpStatus.OK);
		}
			catch (CustomerNotFoundException e) {
				throw e;
			}
			catch (CustomerLoanNotFoundException e) {
				throw e;
			} 
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	
	
	

}
