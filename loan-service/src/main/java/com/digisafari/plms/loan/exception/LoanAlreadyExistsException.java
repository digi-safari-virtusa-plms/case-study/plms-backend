package com.digisafari.plms.loan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Loan Already exists with this details")
public class LoanAlreadyExistsException extends Exception {

}
