package com.digisafari.plms.loan.config;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digisafari.plms.loan.domain.Loan;
import com.digisafari.plms.loan.exception.LoanNotFoundException;
import com.digisafari.plms.loan.service.LoanService;

@Component
public class Consumer {

	@Autowired
	private LoanService loanService;
	
	@RabbitListener(queues = "loans_queue")
	public void getUserDtoFromRabbitMq(Loan loan) throws LoanNotFoundException {
		System.out.println("Inside RabbitMQ consumer for loan::" + loan);
		this.loanService.updateLoan(loan);
	}
	
}
