package com.digisafari.plms.loan.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.digisafari.plms.loan.domain.Loan;

@Repository
public interface LoanRepository extends MongoRepository<Loan, String> {

}
